FROM ubuntu:latest

# Install JDK and sbt
RUN apt-get update && apt-get install -y curl gnupg
RUN echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" > /etc/apt/sources.list.d/sbt.list
RUN echo "deb https://repo.scala-sbt.org/scalasbt/debian /" > /etc/apt/sources.list.d/sbt_old.list
RUN curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | apt-key add
RUN apt-get update && apt-get install -y sbt openjdk-8-jdk

# Install Verilator
RUN apt-get install -y git help2man perl python3 make autoconf g++ flex bison ccache libgoogle-perftools-dev numactl perl-doc
RUN git clone https://github.com/verilator/verilator /tmp/verilator
WORKDIR /tmp/verilator
RUN git checkout v4.202
RUN autoconf && ./configure && make && make install

# Insatll CMake
RUN apt-get install -y cmake

RUN mkdir /work
WORKDIR /work
